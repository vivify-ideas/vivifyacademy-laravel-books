<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $books = DB::table('books')->get();
    return view('welcome', compact('books'));
});

Route::get('/books/{id}', ['as' => 'single-book', 'uses' => function ($id) {
    $book = DB::table('books')->find($id);
    return view('single-book', compact('book'));
}]);

Route::get('about', function () {
    return view('about');
});
